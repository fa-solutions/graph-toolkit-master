let FAClient = null;
let notyf = null;
let currentRecord = null;

const SERVICE = {
    name: 'FreeAgentService',
    appletId: 'aHR0cHM6Ly9ncmFwaC1hcHBsZXQtdGVzdC53ZWIuYXBw',
};

function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    FAClient.on('meeting', ({record}) => {
        let { field_values, id } = record;
        console.log(new Date().getTime())
        console.log(field_values);
        if (field_values.task_field7 && !field_values.task_field7.value) {
            createOnlineMeeting(field_values, id);
        } else {
            if (field_values.task_field7 && field_values.task_field7.value && field_values.task_field7.value == "") {
                createOnlineMeeting(field_values, id);
            }
        }
    })


    FAClient.on('addTask', ({record=null, app="contact"}) => {
        currentRecord = { record, app };
        document.getElementById('one-tab').click();
        let tasksNode = document.querySelector("mgt-tasks");
        if (tasksNode && tasksNode.shadowRoot.querySelector("div.Header > div.AddBarItem.NewTaskButton")) {
            let button = document.querySelector("mgt-tasks").shadowRoot.querySelector("div.Header > div.AddBarItem.NewTaskButton");
            button.click();
            FAClient.open();
        }

        console.log(button);
        console.log(currentRecord);

    });

    FAClient.on('createTask', async ({record}) => {
        if (client && !record.field_values.task_field5.value) {
            let taskRes = await client.api(`/planner/tasks`)
                .post({
                    "planId": "KWoJPOaXCEKt_SJYhV7JvGUABzE3",
                    "bucketId": "r1nY84zVokeFSp2CYtK80mUAMO94",
                    "title": record.field_values.description.value,
                    "assignments": {
                        "1ccfe6ba-c762-49dd-a424-ee461c8d13f1": {
                            "@odata.type": "microsoft.graph.plannerAssignment",
                            "orderHint" : ' !'
                        }
                    },
                    "dueDateTime": record.field_values.due_date.value
                });
            FAClient.open();
            document.getElementById('one-tab').click();
            var tasksList = document.getElementById('tasks-container');
            var tasksEl = document.createElement('mgt-tasks');
            tasksList.innerHTML = '';
            tasksList.appendChild(tasksEl);
            console.log(taskRes);
            let updateOptions = {
                entity: "task",
                id: record.id,
                field_values: {
                    task_field5: taskRes.id
                }
            }
            FAClient.upsertEntity(updateOptions, (res) => {
                console.log(res);
            });
            FAClient.navigateTo(`/task/view/all`)
            FAClient.navigateTo(`/task/view/${record.id}`);
            FAClient.updateEntity( {
                entity: "task",
                id: record.id,
                field_values: {} }, (res) => {
                console.log(res);
            });
        }
    });

    const {
        clientId="MnsjFsJhS-GBUe_kNqLyCQ"
    } = FAClient.params;

}

function createEntity(options) {
    FAClient.upsertEntity(options, (res) => {
        console.log(res);
        FAClient.navigateTo(`/${options.entity}/view/${res.entity_value.id}`);
        FAClient.showModal('entityFormModal', {
            entity: options.entity,
            entityLabel: 'Update User',
            entityInstance: res.entity_value,
            showButtons: false,
        });
    });
}


function createTask(options, createdBy={}) {
    let userId = createdBy && createdBy.user ? createdBy.user.id : null;
    FAClient.listEntityValues(
        {
            entity: "team",
            filters: [
                {
                    field_name: "team_field0",
                    operator: 'includes',
                    values: [userId],
                }
            ],
            limit: 1,
        },
        (records) => {
            console.log(records);
            const record = _.get(records, '[0]');
            if (record) {
                options.field_values.task_field6 = record.id;
            }
            if (currentRecord) {
                options.field_values.fa_entity_reference_id = currentRecord.record.id;
                if (currentRecord.app == 'contact') {
                    options.field_values.fa_entity_id = "ac12096d-027b-57f5-b389-93c1920222a3";
                }
                if (currentRecord.app == 'lead') {
                    options.field_values.fa_entity_id = "d0e7d856-75de-4c75-bb53-704b7448db2b";
                }
            }
            console.log(options);
            FAClient.upsertEntity(options, (res) => {
                console.log(res);
                currentRecord = null;
                if (res) {
                    FAClient.navigateTo(`/${options.entity}/view/${res.entity_value.id}`);
                    FAClient.showModal('entityFormModal', {
                        entity: options.entity,
                        entityLabel: 'Update Task',
                        entityInstance: res.entity_value,
                        showButtons: false,
                    });
                }
            });
        });
}

function checkAndUpsertEntity(options) {
    FAClient.listEntityValues(
        {
            entity: options.upsert.entity,
            filters: options.filters,
            limit: 1,
        },
        (records) => {
            const record = _.get(records, '[0]');
            if (record) {
                options.upsert.id = record.id;
            }
            FAClient.upsertEntity(options.upsert, (res) => {
                console.log(res);
            });
        },
    );
};

function deleteRecord(options) {
    FAClient.listEntityValues(
        {
            entity: options.upsert.entity,
            filters: options.filters,
            limit: 1,
        },
        (records) => {
            const record = _.get(records, '[0]');
            if (record) {
                options.upsert.id = record.id;
                FAClient.updateEntity(options.upsert, (res) => {
                    console.log(res);
                });
            }
        },
    );
};


function createOnlineMeeting(fieldValues, id) {
    let response = null;
    let userId = fieldValues.task_field6 && fieldValues.task_field6.value ? fieldValues.task_field6.value : null;
    FAClient.listEntityValues(
        {
            entity: "team",
            id: userId,
            limit: 1,
        },
        async (records) => {
            const record = _.get(records, '[0]');
            let newEvent = {
                "subject": fieldValues.description.value,
                "body": {
                    "contentType": "HTML",
                    "content": fieldValues.description.value
                },
                "start": {
                    "dateTime": fieldValues.event_start_time.value,
                    "timeZone": "Pacific Standard Time"
                },
                "end": {
                    "dateTime": fieldValues.event_end_time.value,
                    "timeZone": "Pacific Standard Time"
                },
                "isOnlineMeeting": true
            };

            let meetingRes = await client.api(`/me/events`)
                .post(newEvent);
            console.log(meetingRes);


            FAClient.updateEntity( {
                entity: "task",
                id,
                field_values: { task_field7: meetingRes.onlineMeeting.joinUrl }
            }, (res) => {
                console.log(res);
            });
        },
    );
    return response;
}



/*function createOnlineMeeting(fieldValues, id) {
    let response = null;
    FAClient.listEntityValues(
        {
            entity: "team",
            id: fieldValues.task_field2.value,
            limit: 1,
        },
        async (records) => {
            const record = _.get(records, '[0]');
            let  meetingRes = await client.api(`/me/onlineMeetings`)
                .post({
                    "startDateTime": fieldValues.event_start_time.value,
                    "endDateTime": fieldValues.event_end_time.value,
                    "subject": fieldValues.description.value,
                    "participants": {
                        "organizer": {
                            "identity": {
                                "user": {
                                    "id": record.field_values.team_field0.value
                                }
                            }
                        },
                        "attendees": [{
                            "identity": {
                                "user": {
                                    "id": ""
                                }
                            }
                        }]
                    }
                });
            console.log(meetingRes);
            FAClient.updateEntity( {
                entity: "task",
                id,
                field_values: { task_field0: meetingRes.joinUrl }
            }, (res) => {
                console.log(res);
            });
        },
    );
    return response;
}*/






//FAClient.upsertEntity();

/*
function logMessages(messages, phoneNumbers, callback) {
    const messagesIDs = messages.map((m) => `${m.id}`);

    FAClient.listEntityValues(
        {
            entity: PHONE_APPLET_CONFIGURATION.name,
            filters: [
                {
                    field_name: PHONE_APPLET_CONFIGURATION.fields.ringcentralId,
                    operator: 'includes',
                    values: messagesIDs,
                },
            ],
        },
        (existingMessages) => {
            console.log(existingMessages);
            const pattern = parseNumbersForPattern(phoneNumbers);

            FAClient.listEntityValues(
                {
                    entity: currentApp,
                    pattern,
                    limit: 1,
                },
                (contacts) => {
                    const contact = _.get(contacts, '[0]');
                    console.log(contacts);
                },
            );
        }
    );
}
 */

